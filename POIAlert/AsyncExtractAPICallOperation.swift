//
//  AsyncExtractAPICallOperation.swift
//  POIAlert
//

import Foundation

// MARK: - Based on an example from the internet
/**
 Took a really long time to find an example of being able to run this API multiple times asynchronously.
 When we finally found an example of it we quicly implemented it and closed the example.
 We cannot for the life of us find the example, which is understandable, it took a really long to find it
 to begin with. I hope this can be excused but to reiterate this is from an example online.
 The same goes for the AsyncOperation superclass and AsyncCoordAPICallOperation sibling class.
 */

class AsyncExtractAPICallOperation: AsyncOperation {
    
    var results: Result<[String: Any]?, NetworkError>!
    
    var id: String?

    override func main() {
        
        let urlString = "https://en.wikipedia.org//w/api.php?origin=*&action=query&format=json&prop=extracts|info&pageids=\(id ?? "0")&exintro=1&explaintext=1&exsectionformat=wiki&excontinue=&inprop=url"
        let component = transformURLString(urlString)
        guard let url = component?.url else { return }
        
        var info = [String: Any]()
        
        let defaultSession = URLSession(configuration: .default)
        let dataTask = defaultSession.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print("Image download encountered an error: \(error.localizedDescription)")
            } else if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                if self.isCancelled {
                    self.state = .finished
                    return
                }
                
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                    guard let jsonArray = jsonResponse as? [String: Any] else { return }
                    guard let query = jsonArray["query"]! as? [String: Any] else { return }
                    guard let pages = query["pages"]! as? [String: Any] else { return }
                    guard let info0 = pages[self.id!]! as? [String: Any] else { return }
                    info = info0
        
                } catch let parsingError {
                    self.results = .failure(.server)
                    print("Error", parsingError)
                }
                
                self.results = .success(info)
                self.state = .finished
            }
        }
        dataTask.resume()
    }
    
    /**
    Helper function. Trying to use only URL() on the urlString gave an unreachable url, most likely because of the "|" character.
    Therefore we use this function to format the url.
    */
    func transformURLString(_ string: String) -> URLComponents? {
        guard let urlPath = string.components(separatedBy: "?").first else {
            return nil
        }
        var components = URLComponents(string: urlPath)
        if let queryString = string.components(separatedBy: "?").last {
            components?.queryItems = []
            let queryItems = queryString.components(separatedBy: "&")
            for queryItem in queryItems {
                guard let itemName = queryItem.components(separatedBy: "=").first,
                    let itemValue = queryItem.components(separatedBy: "=").last else {
                        continue
                }
                components?.queryItems?.append(URLQueryItem(name: itemName, value: itemValue))
            }
        }
        return components!
    }
    
    enum NetworkError: Error {
        case url
        case server
    }
}



