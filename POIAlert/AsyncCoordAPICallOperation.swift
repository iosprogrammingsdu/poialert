//
//  AsyncCoordAPICallOperation.swift
//  POIAlert
//

import Foundation

// MARK: - Based on an example from the internet
/**
 Took a really long time to find an example of being able to run this API multiple times asynchronously.
 When we finally found an example of it we quicly implemented it and closed the example.
 We cannot for the life of us find the example, which is understandable, it took a really long to find it
 to begin with. I hope this can be excused but to reiterate this is from an example online.
 The same goes for the AsyncOperation superclass and AsyncEctractAPICallOperation sibling class.
 */

class AsyncCoordAPICallOperation: AsyncOperation {
    
    var results: Result<[[String: Any]]?, NetworkError>!
    
    var latitude: String?
    var longitude: String?
    var radius: String?

    override func main() {
        
        let urlString = "https://en.wikipedia.org/w/api.php?origin=*&action=query&list=geosearch&gscoord=\(latitude ?? "0.0")|\(longitude ?? "0.0")&gsradius=\(radius ?? "1000")&gslimit=10&format=json"
        let component = transformURLString(urlString)
        guard let url = component?.url else { return }
        
        var places = [[String: Any]]()
        
        let defaultSession = URLSession(configuration: .default)
        let dataTask = defaultSession.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print("Image download encountered an error: \(error.localizedDescription)")
            } else if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                if self.isCancelled {
                    self.state = .finished
                    return
                }
                
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                    guard let jsonArray = jsonResponse as? [String: Any] else { return }
                    guard let query = jsonArray["query"]! as? [String: Any] else { return }
                    guard let geosearch = query["geosearch"]! as? [Any] else { return }
                    
                    for obj in geosearch {
                        guard let place = obj as? [String: Any] else { return }
                        places.append(place)
                    }
                } catch let parsingError {
                    self.results = .failure(.server)
                    print("Error", parsingError)
                }
                
                self.results = .success(places)
                self.state = .finished
            }
        }
        dataTask.resume()
    }
    
    /**
     Helper function. Trying to use only URL() on the urlString gave an unreachable url, most likely because of the "|" character.
     Therefore we use this function to format the url.
     */
    private func transformURLString(_ string: String) -> URLComponents? {
        guard let urlPath = string.components(separatedBy: "?").first else {
            return nil
        }
        var components = URLComponents(string: urlPath)
        if let queryString = string.components(separatedBy: "?").last {
            components?.queryItems = []
            let queryItems = queryString.components(separatedBy: "&")
            for queryItem in queryItems {
                guard let itemName = queryItem.components(separatedBy: "=").first,
                    let itemValue = queryItem.components(separatedBy: "=").last else {
                        continue
                }
                components?.queryItems?.append(URLQueryItem(name: itemName, value: itemValue))
            }
        }
        return components!
    }
    
    enum NetworkError: Error {
        case url
        case server
    }
}



