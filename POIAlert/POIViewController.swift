//
//  POIViewController.swift
//  POIAlert
//

import UIKit

class POIViewController: UIViewController {

    // MARK: - Variables
    
    @IBOutlet weak var poiTitle: UILabel!
    @IBOutlet weak var poiText: UILabel!
    var titleText: String? = "Loading title..."
    var textText: String? = "Loading extract..."
    var urlString: String!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        poiTitle.text = titleText
        poiText.text = textText
        poiText.sizeToFit()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Go to Web View" {
            if let wwvc = segue.destination as? WebViewViewController {
                wwvc.wikiURLString = urlString
            }
        }
    }
    
    // MARK: - Other methods
    
    /**
     Update the view
     */
    func updateView(title: String, text: String, url: String) {
        titleText = title
        poiTitle.text = titleText
        textText = text
        poiText.text = textText
        urlString = url
        print(text)
        self.view.setNeedsLayout()
    }
}
