//
//  ViewController.swift
//  POIAlert
//

import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
    
    // MARK: - Constants
    
    let locationManager = CLLocationManager()
    private let RADIUS = 10000.0
    private let GEO_FENCE_ID = "geofenceregion"
    
    // MARK: - Variables
    
    private var currentMopoisvc: MapOfPOIsViewController?
    private var currentLopoisvc: ListOfPOIsViewController?
    private var places = [[String: Any]]() {
        didSet {
            currentMopoisvc?.updateView(places: places)
            currentLopoisvc?.updateView(places: places)
        }
    }
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    private var geoFenceRegion: CLCircularRegion?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkLocationServices()
        updateGeoFencing()
    }
    
    // MARK: - Location methods
    
    /**
     Update the geofencing of the user.
     */
    private func updateGeoFencing() {
        if let coords = locationManager.location?.coordinate {
            geoFenceRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(coords.latitude, coords.longitude), radius: RADIUS, identifier: GEO_FENCE_ID)
            if let gfr = geoFenceRegion {
                locationManager.startMonitoring(for: gfr)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        updateGeoFencing()
        setPlaces()
    }
    
    /**
     Check location services
     */
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            checkLocationAuthorization()
        }
    }
    
    /**
     Appoint the delegate of the locationManager as this object
     */
    private func setupLocationManager() {
        locationManager.delegate = self
    }
    
    /**
     Check the current authorization
     */
    private func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            redirectUserToSettingsPrompt()
            break
        case .denied:
            redirectUserToSettingsPrompt()
            break
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .restricted:
            locationManager.requestAlwaysAuthorization()
            redirectUserToSettingsPrompt()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        @unknown default:
            break
        }
    }
    
    /**
     Alert the user that he must change his privacy settings so the app will work as intended
     */
    private func redirectUserToSettingsPrompt() {
        let alertController = UIAlertController(title: "We need your location", message: "Please go to Settings > Privacy > Location Services > POIAlert and allow location access always for best use of app", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
             }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)

        // check the permission status
        switch(CLLocationManager.authorizationStatus()) {
            case .authorizedAlways, .authorizedWhenInUse:
                print("Authorized.")
            case .notDetermined, .restricted, .denied:
                // redirect the users to settings
                self.present(alertController, animated: true, completion: nil)
            @unknown default:
                fatalError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    // MARK: - Notifications
    
    /**
     Schedule the notifications of the current places
     */
    private func scheduleNotifications() {
        for place in places {
            let title = place["title"] as! String
            print("\(place["lat"] as! Double) \(place["lon"] as! Double)")
            self.appDelegate?.scheduleLocationNotification(withTitle: title, withBody: "\(title) is nearby", withLat: place["lat"] as! Double, withLon: place["lon"] as! Double)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Go to Map" {
            if let mopoivc = segue.destination as? MapOfPOIsViewController {
                currentMopoisvc = mopoivc
                setPlaces()
            }
        } else if segue.identifier == "Go to Scroll" {
            if let lopoisvc = segue.destination as? ListOfPOIsViewController {
                currentLopoisvc = lopoisvc
                setPlaces()
            }
        }
    }
    
    // MARK: - API Call
    
    /**
     Set the places we get from the API call to the places variable
     */
    func setPlaces() {
        
        guard let location = self.locationManager.location?.coordinate else { return }
        
        let operation = AsyncCoordAPICallOperation()
        operation.radius = "\(RADIUS)"
        operation.latitude = "\(location.latitude as Double)"
        operation.longitude = "\(location.longitude as Double)"
        
        operation.completionBlock = {
            DispatchQueue.main.async {
                switch operation.results {
                case let .success(data):
                    self.places = data!
                    print(self.places)
                    self.scheduleNotifications()
                case let .failure(error):
                    print(error)
                case .none:
                    break
                }
            }
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operation)
    }
}

