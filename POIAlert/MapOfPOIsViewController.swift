//
//  MapOfPOIsViewController.swift
//  POIAlert
//

import UIKit
import MapKit
import CoreLocation

/**
 Controller of the Map View
 Delegate of CLLocationManager
 */
class MapOfPOIsViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: - Constants
    
    let locationManager = CLLocationManager()
    let regionInMeters = 250.0
    
    // MARK: - Variables
    
    @IBOutlet private weak var mapView: MKMapView!
    private var defaultCoordLabelText = "No coordinates received"
    lazy var coordLabelText: String = defaultCoordLabelText
    lazy var places = [[String: Any]]()
    private var currentPoivc: POIViewController?
    private var info = [String: Any]() {
        didSet {
            currentPoivc?.updateView(title: info["title"] as? String ?? "no title", text: info["extract"] as? String ?? "no extract", url: info["fullurl"] as? String ?? "https://www.wikipedia.org")
        }
    }
    private var allAnnotations: [MKAnnotation]?
    private var displayedAnnotations: [MKAnnotation]? {
        willSet {
            if let currentAnnotations = displayedAnnotations {
                mapView.removeAnnotations(currentAnnotations)
            }
        }
        didSet {
            if let newAnnotations = displayedAnnotations {
                mapView.addAnnotations(newAnnotations)
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerMapAnnotationViews()
        
        var annos = [MKAnnotation]()
        
        for place in places {
            let wikiAnn = WikiAnnotation(latitude: place["lat"] as? Double ?? 0.0, longitude: place["lon"] as? Double ?? 0.0, title: place["title"] as? String ?? "Title not found", subtitle: "point of interest")
            annos.append(wikiAnn)
        }
        
        allAnnotations = annos
        showAllAnnotations(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(coordLabelText)
        centerViewOnUserLocation()

        checkLocationServices()
    }
    
    // MARK: - Location methods
    
    /**
     Center the view on the current location of the user on the map
     */
    private func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    /**
     Check the location services
     */
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            checkLocationAuthorization()
        }
    }
    
    /**
     Set the the controller as a delegate for the locationManager
     */
    private func setupLocationManager() {
        locationManager.delegate = self
    }
    
    /**
     Check the current location authorization.
     */
    private func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            break
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        @unknown default:
            break
        }
    }
   
    // MARK: - Annotation methods
    
    /**
     Register the views of the Map Annotations to the mapView
     */
    private func registerMapAnnotationViews() {
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: NSStringFromClass(WikiAnnotation.self))
    }
    
    /**
     Show all of the current annotations
     */
    private func showAllAnnotations(_ sender: Any) {
        displayedAnnotations = allAnnotations
    }
    
    // MARK: - API Call
    
    /**
     Set the info of the selected annotation to be passed to the next view
     */
    private func setInfo(title: String) {
        
        let operation = AsyncExtractAPICallOperation()
        var place = [String: Any]()
        for p in places {
            if p["title"] as! String == title {
                place = p
            }
        }
        operation.id = "\(place["pageid"] as! Int)"
        
        operation.completionBlock = {
            DispatchQueue.main.async {
                switch operation.results {
                case let .success(data):
                    print(data!)
                    self.info = data!
                case let .failure(error):
                    print(error)
                case .none:
                    break
                }
            }
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operation)

    }
    
    // MARK: - Other methods
    
    /**
     Update the view
     */
    func updateView(places: [[String: Any]]) {
        self.places = places
        viewDidLoad()
    }
}

extension MapOfPOIsViewController: MKMapViewDelegate {
    
    // MARK: - Map View methods
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if let annotation = view.annotation, annotation.isKind(of: WikiAnnotation.self) {
            
            if let poiController = storyboard?.instantiateViewController(withIdentifier: "poiV") as? POIViewController {
                currentPoivc = poiController
                setInfo(title: annotation.title!!)
                
                let navController = UINavigationController()
                navController.modalPresentationStyle = .popover
                
                navController.addChild(currentPoivc!)
                
                let presentationController = navController.popoverPresentationController
                presentationController?.permittedArrowDirections = .any
                
                // Anchor the popover to the button that triggered the popover.
                presentationController?.sourceRect = control.frame
                presentationController?.sourceView = control
                
                present(navController, animated: true, completion: nil)
            }
        }
    }
 
    // MARK: - From an example from Apple
    // link: https://developer.apple.com/documentation/mapkit/mapkit_annotations/annotating_a_map_with_custom_data
    
    /// The map view asks `mapView(_:viewFor:)` for an appropiate annotation view for a specific annotation.
    /// - Tag: CreateAnnotationViews
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }
        
        var annotationView: MKAnnotationView?
        
        if let annotation = annotation as? WikiAnnotation {
            annotationView = setupWikiAnnotationView(for: annotation, on: mapView)
        }
        
        return annotationView
    }
    
    /// Create an annotation view for the Golden Gate Bridge, customize the color, and add a button to the callout.
    /// - Tag: CalloutButton
    private func setupWikiAnnotationView(for annotation: WikiAnnotation, on mapView: MKMapView) -> MKAnnotationView {
        let identifier = NSStringFromClass(WikiAnnotation.self)
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
        if let markerAnnotationView = view as? MKMarkerAnnotationView {
            markerAnnotationView.animatesWhenAdded = true
            markerAnnotationView.canShowCallout = true
            markerAnnotationView.markerTintColor = UIColor(named: "internationalOrange")
            let rightButton = UIButton(type: .detailDisclosure)
            markerAnnotationView.rightCalloutAccessoryView = rightButton
        }
        
        return view
    }
}
