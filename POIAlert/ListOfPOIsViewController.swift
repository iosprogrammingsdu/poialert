//
//  ListOfPOIsViewController.swift
//  POIAlert
//

import UIKit
import CoreLocation

class ListOfPOIsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    // MARK: - Variables
    
    @IBOutlet weak var tableView: UITableView!
    private var selectedCellIndex: Int!
    var locationManager = CLLocationManager()
    lazy var places = [[String: Any]]()
    private var currentPoivc: POIViewController?
    private var info = [String: Any]() {
        didSet {
            currentPoivc?.updateView(title: info["title"] as? String ?? "no title", text: info["extract"] as? String ?? "no extract", url: info["fullurl"] as? String ?? "https://www.wikipedia.org")
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table View methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let prototype = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: prototype, for: indexPath)
        cell.textLabel?.text = places[indexPath.row]["title"] as? String
        
        cell.detailTextLabel?.text = String(format: "%.1f m", places[indexPath.row]["dist"] as? Double ?? "??")
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCellIndex = indexPath.row
        performSegue(withIdentifier: "Go to POI", sender: tableView)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let poivc = segue.destination as? POIViewController {
            currentPoivc = poivc
            setInfo()
        }
    }
    
    // MARK: - API Call
    
    /**
     Set the info of the selected annotation to be passed to the next view
     */
    func setInfo() {
        
        let operation = AsyncExtractAPICallOperation()
        operation.id = "\(places[selectedCellIndex]["pageid"] as! Int)"
        
        operation.completionBlock = {
            DispatchQueue.main.async {
                switch operation.results {
                case let .success(data):
                    print(data!)
                    self.info = data!
                case let .failure(error):
                    print(error)
                case .none:
                    break
                }
            }
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operation)
    }
    
    // MARK: - Other methods
    
    /**
     Update the view
     */
    func updateView(places: [[String: Any]]) {
        self.places = places
        tableView.reloadData()
    }
}


