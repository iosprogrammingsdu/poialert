//
//  AsyncOperation.swift
//  POIAlert
//

import Foundation

// MARK: - Based on an example from the internet
/**
 Took a really long time to find an example of being able to run this API multiple times asynchronously.
 When we finally found an example of it we quicly implemented it and closed the example.
 We cannot for the life of us find the example, which is understandable, it took a really long to find it
 to begin with. I hope this can be excused but to reiterate this is from an example online.
 The same goes for the AsyncCoordAPICallOperation and AsyncEctractAPICallOperation subclasses.
 */

class AsyncOperation: Operation {
    public enum State: String {
        case ready, executing, finished

        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }

    public var state = State.ready {
        willSet {
            willChangeValue(forKey: state.keyPath)
            willChangeValue(forKey: newValue.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }
}

extension AsyncOperation {

    override var isAsynchronous: Bool {
        return true
    }

    override var isExecuting: Bool {
        return state == .executing
    }

    override var isFinished: Bool {
        return state == .finished
    }

    override func start() {
        if isCancelled {
            return
        }
        main()
        state = .executing
    }
}
