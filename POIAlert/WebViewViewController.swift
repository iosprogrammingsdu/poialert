//
//  WebViewViewController.swift
//  POIAlert
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKUIDelegate {

    // MARK: - Variables
    
    @IBOutlet weak var webView: WKWebView!
    var wikiURLString: String!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myURL = URL(string: wikiURLString)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
}
