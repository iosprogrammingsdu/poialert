//
//  WikiAnnotation.swift
//  POIAlert
//

import Foundation
import MapKit

// MARK: -Based on an example from Apple, same as in MapOfPOIsViewController
// https://developer.apple.com/documentation/mapkit/mapkit_annotations/annotating_a_map_with_custom_data
class WikiAnnotation: NSObject, MKAnnotation {
    
    // MARK: - Variables
    @objc dynamic var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    // MARK: - Methods
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees, title wikiTitle: String, subtitle wikiSubtitle: String) {
        coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        title = wikiTitle
        subtitle = wikiSubtitle
        super.init()
    }
}
